module.exports = { // adapted from: https://git.io/vodU0
    'Los estudiantes login falied': function(browser) {
      browser
        .url('https://losestudiantes.co/')
        .click('.botonCerrar')
        .waitForElementVisible('.botonIngresar', 4000)
        .click('.botonIngresar')
        .setValue('.cajaLogIn input[name="correo"]', 'wrongemail@example.com')
        .setValue('.cajaLogIn input[name="password"]', '1234')
        .click('.cajaLogIn .logInButton')
        .waitForElementVisible('.aviso.alert.alert-danger', 4000)
        .assert.containsText('.aviso.alert.alert-danger', 'El correo y la contraseña que ingresaste no figuran')
    },
    'ir a la página de un profesor': function(browser){
        browser.url('https://losestudiantes.co')
        .waitForElementVisible('.profesores', 4000)
        .click('.profesores .profesor img')
        .waitForElementVisible('.descripcionProfesor', 4000)
        .expect.element('.descripcionProfesor').to.be.visible;
    },
    'usar filtros de cursos':function(browser){
      browser.url('https://losestudiantes.co')
        .waitForElementVisible('.profesores', 4000)
        .click('.profesores .profesor img')
        .waitForElementVisible('.descripcionProfesor', 4000)
        .click('.materias input[type="checkbox"]')
	.end();;
    }
  };
