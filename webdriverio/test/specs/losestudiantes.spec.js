var assert = require('assert');
describe('los estudiantes login', function() {
    it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');

        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

        passwordInput.click();
        passwordInput.keys('1234');

        cajaLogIn.waitForVisible('button=Ingresar', 5000);
        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('.aviso.alert.alert-danger');

        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    });

    describe('los estudiantes valid login', function() {
        it('Visists los estudiantes and succed at login', function(){
            browser.url('https://losestudiantes.co');
            browser.waitForVisible('button=Ingresar', 5000);
            browser.click('button=Ingresar');

            var cajaLogIn = browser.element('.cajaLogIn');
            var mailInput = cajaLogIn.element('input[name="correo"]');

            mailInput.click();
            mailInput.keys('wrongemail2@example.com');

            var passwordInput = cajaLogIn.element('input[name="password"]');

            passwordInput.click();
            passwordInput.keys('12345678');

            cajaLogIn.waitForVisible('button=Ingresar', 5000);
            cajaLogIn.element('button=Ingresar').click()
        });
    });

    describe('los estudiantes invalid signup', function() {
        it('Visists los estudiantes and fail at signup', function(){
            browser.url('https://losestudiantes.co');
            browser.waitForVisible('button=Ingresar', 5000);
            browser.click('button=Ingresar');

            var cajaLogIn = browser.element('.cajaSignUp');

            var nameInput = cajaLogIn.element('input[name="nombre"]');
            var apeInput = cajaLogIn.element('input[name="apellido"]');
            var mailInput = cajaLogIn.element('input[name="correo"]');
            var passInput = cajaLogIn.element('input[name="password"]');

            nameInput.click();
            nameInput.keys('wronge');

            apeInput.click();
            apeInput.keys('wronge');
            
            mailInput.click();
            mailInput.keys('wrongemail2@example.com');

            var checks = cajaLogIn.element('input[type="checkbox"]');
            checks.click();
            
            cajaLogIn.selectByVisibleText('select[name="idUniversidad"]','Universidad de los Andes');

            passInput.click();
            passInput.keys('12345678');

            cajaLogIn.selectByVisibleText('select[name="idPrograma"]','Maestría en Ingeniería de Software');

            cajaLogIn.waitForVisible('button=Registrarse', 5000);
            cajaLogIn.element('button=Registrarse').click()
        });
    });
});