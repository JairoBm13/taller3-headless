import { TourOfHeroesPage } from './app.po';

describe('Tour of heroes Dashboard', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('should display top 4 heroes', () => {
    page.navigateTo();
    expect(page.getTop4Heroes()).toEqual(['Mr. Nice', 'Narco', 'Bombasto', 'Celeritas']);
  });

  it('should navigate to heroes', () => {
    page.navigateToHeroes();
    expect(page.getAllHeroes().count()).toBe(11);
  });
});

describe('Tour of heroes, heroes page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should add a new hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.enterNewHeroInInput('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n + 1));
  });

});

describe('Tour of heroes, search hero', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateTo();
  });

  it('should find a hero', () => {
    page.serchForHeroByName("Bombasto");
    expect(page.lookForResultsHero().count()).toBeGreaterThan(0);
  });

  it('should not find inexistent hero', () => {
    page.serchForHeroByName("My new hero");
    expect(page.lookForResultsHero().count()).toBe(0);
  });
});

describe('Tour of heroes, delete hero',() => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should not find deleted hero', ()=> {
    const currentHeroes = page.getAllHeroes().count();
    page.deleteHero(2);
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n -1));
  });
});

describe('Tour of heroes, navigate to hero',() => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateTo();
  });

  it('should reach a hero from dashboard', ()=> {
    page.selectTopHero(0)
  });

  it('should reach a hero from heroes list', ()=> {
    page.navigateToHeroes();
    page.selectHero(6);
  });

  it('should reach a hero from search', ()=> {
    page.serchForHeroByName("Bombasto");
    page.clickSearchedHero();
  });
});